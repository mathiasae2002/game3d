using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class GetRankingController : MonoBehaviour
{

    public void GetRanking(Action<RankingDataModel> OnFinish)
    {
        StartCoroutine(GetRankingRequest(OnFinish));
    }

    IEnumerator GetRankingRequest(Action<RankingDataModel> OnFinish)
    {
        using (UnityWebRequest www = UnityWebRequest.Get("http://localhost/progra/Ranking.php"))
        {
            yield return www.SendWebRequest();

            if (www.result == UnityWebRequest.Result.ProtocolError ||
                www.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log("Error!");
            }
            else
            {
                RankingDataModel rankingData=JsonUtility.FromJson<RankingDataModel>(www.downloadHandler.text);
                OnFinish?.Invoke(rankingData);
            }
        }
    }
}
