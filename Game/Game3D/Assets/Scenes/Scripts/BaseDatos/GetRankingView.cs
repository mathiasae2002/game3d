using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class GetRankingView : MonoBehaviour
{
    [SerializeField] private Transform container;
    [SerializeField] private GameObject textPrefab;
    private GetRankingController getRankingController;

    void Awake()
    {
        getRankingController=GetComponent<GetRankingController>();
        GetRanking();
    }

    public void GetRanking()
    {
        getRankingController.GetRanking(ShowRanking);
    }

    private void ShowRanking(RankingDataModel rankingData)
    {
        Transform[] transforms=container.GetComponentsInChildren<Transform>();
        foreach (Transform t in transforms)
        {
            if (t != container.transform)
            {
                Destroy(t.gameObject);
            }
        }

        foreach(RankingUserModel user in rankingData.data)
        {
            GameObject text = Instantiate(textPrefab, container);
            text.GetComponent<TextMeshProUGUI>().text = $"{user.nombre} - {user.puntaje}";
        }
    }
}
