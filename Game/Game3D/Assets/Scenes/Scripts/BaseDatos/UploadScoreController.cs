using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class UploadScoreController : MonoBehaviour
{
    
    public void UploadScore(string nombre, int puntaje, Action OnFinish)
    {
        StartCoroutine(UploadScoreRequest(nombre,puntaje, OnFinish));
    }

    IEnumerator UploadScoreRequest(string nombre, int puntaje, Action OnFinish)
    {
        WWWForm form = new WWWForm();
        form.AddField("nombre", nombre);
        form.AddField("puntaje", puntaje);
        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost/progra/UpdateRecord.php",form))
        {
            yield return www.SendWebRequest();

            if(www.result==UnityWebRequest.Result.ProtocolError ||
                www.result == UnityWebRequest.Result.ConnectionError)
            {
                Debug.Log("Error!");
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                OnFinish?.Invoke();
            }
        }
    }
}
