using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
public class UploadScoreView : MonoBehaviour
{
    [SerializeField] private TMP_InputField nombreInputField;
    [SerializeField] private TMP_InputField puntajeInputField;
    [SerializeField] private Button uploadScoreButton;
    private UploadScoreController uploadScoreController;
    [SerializeField] private GetRankingView rankingView;

    private void Awake()
    {
        uploadScoreController = GetComponent<UploadScoreController>();
        uploadScoreButton.onClick.AddListener(UploadScore);
    }

    private void UploadScore()
    {
        uploadScoreController.UploadScore(
            nombreInputField.text, 
            int.Parse(puntajeInputField.text),
            rankingView.GetRanking);
    }
}
