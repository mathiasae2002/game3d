using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CambiosEntrePantallas : MonoBehaviour
{
    public GameObject objeto;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Activador()
    {
        objeto.SetActive(true);
    }
    public void Desactivador()
    {
        objeto.SetActive(false);
        Time.timeScale = 1f;
    }
}
