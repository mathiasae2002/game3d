using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEstatus : MonoBehaviour
{
    public GameObject muerte;
    public GameObject victoria;
    public float vida=1;
    public float puntaje=0;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (vida == 0)
        {
            muerte.SetActive(true);
            Time.timeScale = 0f;
        }
        if(victoria==true)
        {
            victoria.SetActive(true);
            Time.timeScale=0f;
        }
    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == "Enemigo")
        {
            vida =0;
        }
        else if(col.gameObject.tag == "Puntos")
        {
            puntaje = puntaje+100;
        }
    }
}
