using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovimiento : MonoBehaviour
{
    private Rigidbody rb;
    public float velocidad;
    public float fuerzasalto;
    public bool Saltando;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Mover();

        if (Input.GetKeyDown(KeyCode.Space)){
            Saltar();
        }

    }

    public void Saltar()
    {
        Vector3 piso=transform.TransformDirection(Vector3.down);

        if(Physics.Raycast(transform.position,piso,1.03f))
        {
            Saltando=true;
        }
        else{
            Saltando=false;
        }

        if(Saltando==true){
        rb.AddForce(new Vector3 (0,fuerzasalto,0),ForceMode.Impulse);
        }
    }
    public void Mover()
    {
        float ver = Input.GetAxis("Horizontal");
        float hor = Input.GetAxis("Vertical");

        if(hor != 0 || ver !=0)
        {
            Vector3 direccion = new Vector3(-hor,0,ver);
            transform.Translate(direccion*velocidad*Time.deltaTime);   
        }

    }
}
